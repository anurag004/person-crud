package com.crud.demo.model;

import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "Person")
public class Person {
    @Transient
    public static final String SEQUENCE_NAME = "person_sequences";
    @Id
    private int id;

    @NonNull
    private String fname;

    @NonNull
    private String lname;

    @Indexed(unique = true)
    private String email;

    @NonNull
    private int age;

    @NonNull
    private Address address;

    public Person(int id, @NonNull String fname, @NonNull String lname, String email, int age, Address address) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.age = age;
        this.address=address;
    }

    @NonNull
    public Address getAddress() {
        return address;
    }

    public void setAddress(@NonNull Address address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getFname() {
        return fname;
    }

    public void setFname(@NonNull String fname) {
        this.fname = fname;
    }

    @NonNull
    public String getLname() {
        return lname;
    }

    public void setLname(@NonNull String lname) {
        this.lname = lname;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age>0)
            this.age = age;
    }
}
