package com.crud.demo.model;

public class Address {
    public String streetName;
    public int streetNumber;
    public String state;

    public Address(String streetName, int streetNumber, String state) {
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.state = state;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
