package com.crud.demo.controller;
import java.util.*;

import com.crud.demo.model.Person;
import com.crud.demo.repository.PersonRepository;
import com.crud.demo.service.PersonSequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@RestController
@RequestMapping("/api/persons")
public class PersonController {
    @Autowired
    PersonSequenceGeneratorService personSequenceGeneratorService;
    @Autowired
    PersonRepository personRepository;

    @GetMapping("")
    public ResponseEntity getAllPerson(){
        List<Person> personList = personRepository.findAll();
        return ResponseEntity.ok().body(personList);
    }
    @PostMapping("")
    public ResponseEntity addPerson(@RequestBody Person person){
        try{
            person.setId((int) personSequenceGeneratorService.generateSequence(Person.SEQUENCE_NAME));
            Person personCreated = personRepository.save(person);
            return ResponseEntity.ok().body(personCreated);
        }catch (Exception exception){
            System.out.println(exception.getMessage());
            return ResponseEntity.status(401).body("User with same email already exists!");
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity updatePerson(@RequestBody Person person,@PathVariable("id") String id){
        try{
            Optional<Person> foundPerson = personRepository.findById(Integer.parseInt(id));
            if(foundPerson.isPresent()){
                Person fperson = foundPerson.get();
                fperson.setEmail(person.getEmail());
                fperson.setAge(person.getAge());
                fperson.setFname(person.getFname());
                fperson.setLname(person.getLname());
                fperson.setAddress(person.getAddress());
                personRepository.save(fperson);
                return ResponseEntity.ok().body(fperson);
            }else{
                throw new Exception("Id not found!");
            }
        }catch (Exception e){
            return ResponseEntity.status(400).body(e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity deletePerson(@PathVariable("id") String id){
        try{
            Optional<Person> foundPerson = personRepository.findById(Integer.parseInt(id));
            if(foundPerson.isPresent()){
                personRepository.delete(foundPerson.get());
                return ResponseEntity.ok().build();
            }else{
                throw new Exception("Id not found!");
            }
        }catch (Exception e){
            return ResponseEntity.status(400).body(e.getMessage());
        }
    }
    @GetMapping("/canVote")
    public ResponseEntity getListOfPersonEliglibleForVoting(){
        try{
            List<Person> personList = personRepository.findAll();
            List<Person> eliglibleList = new ArrayList<>();
            List<Person> notEligibleList = new ArrayList<>();
            for(Person person: personList){
                if(person.getAge() >= 18){
                    eliglibleList.add(person);
                }else{
                    notEligibleList.add(person);
                }
            }
            Map<String, List> finalMap = new HashMap<>();
            finalMap.put("eligible", eliglibleList);
            finalMap.put("non-eligible", notEligibleList);
            return ResponseEntity.ok().body(finalMap);
        }catch (Exception e){
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }
}
