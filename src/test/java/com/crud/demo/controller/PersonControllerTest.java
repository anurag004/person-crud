package com.crud.demo.controller;

import com.crud.demo.model.Address;
import com.crud.demo.model.Person;
import com.crud.demo.repository.PersonRepository;
import com.crud.demo.service.PersonSequenceGeneratorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    PersonSequenceGeneratorService personSequenceGeneratorService;
    @MockBean
    private PersonRepository personRepository;

    @Test
    public void testGetAllPerson() throws Exception {
        // Create a list of Person objects to be returned by the mocked repository
        this.mockMvc.perform(get("/api/persons")).andExpect(status().isOk())
                .andExpect(content().contentType("application/json"));
    }
    @Test
    public void testAddPerson() throws Exception {
        // Create a new Person object to be added to the repository
        Person personToAdd = new Person(1,"John", "Doe", "johndoe@example.com",22,new Address("s1",1,"KA"));

        // Set up the mock sequence generator to return a value of 1
        when(personSequenceGeneratorService.generateSequence(Person.SEQUENCE_NAME)).thenReturn(1L);

        // Set up the mock repository to return the Person object with the generated ID
        when(personRepository.save(any(Person.class))).thenAnswer((Answer<Person>) invocation -> {
            Person person = invocation.getArgument(0);
            person.setId(1);
            return person;
        });

        // Perform the POST request to the "" endpoint with the Person object in the request body
        mockMvc.perform(post("/api/persons")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(personToAdd)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.fname").value("John"))
                .andExpect(jsonPath("$.lname").value("Doe"))
                .andExpect(jsonPath("$.email").value("johndoe@example.com"));

        // Verify that the mock sequence generator and repository were called with the correct arguments
        verify(personSequenceGeneratorService, times(1)).generateSequence(Person.SEQUENCE_NAME);
        verify(personRepository, times(1)).save(any(Person.class));
    }
    @Test
    public void testAddPersonWithExistingEmail() throws Exception {
        // Create a new Person object with an email that already exists in the repository
        Person personToAdd = new Person(1, "John", "Doe", "johndoe@example.com", 22,new Address("s1",1,"KA"));

        // Set up the mock repository to throw an exception indicating that a Person with the same email already exists
        when(personRepository.save(any(Person.class))).thenThrow(new DataIntegrityViolationException("Duplicate email"));

        // Perform the POST request to the "" endpoint with the Person object in the request body
        mockMvc.perform(post("/api/persons")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(personToAdd)))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$").value(("User with same email already exists!")));

        // Verify that the mock repository was called with the correct argument
        verify(personRepository, times(1)).save(any(Person.class));
    }
    @Test
    public void testUpdatePerson() throws Exception {
        // Create a new Person object to be updated in the repository
        Person personToUpdate = new Person(1,"John", "Doe", "johndoe@example.com",22,new Address("s1",1,"KA"));
        personToUpdate.setId(1);

        // Set up the mock repository to return the Person object with the specified ID when it is called with that ID
        when(personRepository.findById(1)).thenReturn(Optional.of(personToUpdate));

        // Update the Person object's email and age
        personToUpdate.setEmail("updated@example.com");
        personToUpdate.setAge(30);

        // Perform the PUT request to the "/api/persons/1" endpoint with the updated Person object in the request body
        mockMvc.perform(put("/api/persons/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(personToUpdate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.fname").value(("John")))
                .andExpect(jsonPath("$.lname").value("Doe"))
                .andExpect(jsonPath("$.email").value("updated@example.com"))
                .andExpect(jsonPath("$.age").value((30)));

        // Verify that the mock repository was called with the correct argument
        verify(personRepository, times(1)).findById(1);
        verify(personRepository, times(1)).save(any(Person.class));
    }

    @Test
    public void testUpdatePersonWithInvalidId() throws Exception {
        // Create a new Person object to be updated in the repository
        Person personToUpdate = new Person(1,"John", "Doe", "johndoe@example.com",22, new Address("s1",1,"KA"));
        personToUpdate.setId(1);

        // Set up the mock repository to return an empty Optional when it is called with the specified ID
        when(personRepository.findById(1)).thenReturn(Optional.empty());

        // Perform the PUT request to the "/api/persons/1" endpoint with the updated Person object in the request body
        mockMvc.perform(put("/api/persons/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(personToUpdate)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("Id not found!"));

        // Verify that the mock repository was called with the correct argument
        verify(personRepository, times(1)).findById(1);
        verify(personRepository, never()).save(any(Person.class));
    }

    @Test
    public void testDeletePersonSuccess() throws Exception {
        // Mocking the repository response
        when(personRepository.findById(1)).thenReturn(Optional.of(new Person(1,"John", "Doe", "johndoe@example.com",22, new Address("s1",1,"KA"))));

        // Perform the DELETE request
        mockMvc.perform(delete("/api/persons/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeletePersonNotFound() throws Exception {
        // Mocking the repository response
        when(personRepository.findById(1)).thenReturn(Optional.empty());

        // Perform the DELETE request
        mockMvc.perform(delete("/api/persons/1"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Id not found!"));
    }

    @Test
    public void testDeletePersonInvalidId() throws Exception {
        // Perform the DELETE request with an invalid ID
        mockMvc.perform(delete("/api/persons/invalid"))
                .andExpect(status().isBadRequest());
    }
    @Test
    public void testGetListOfPersonEligibleForVoting() throws Exception {
        // Create a list of Person objects with varying ages
        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1,"John", "Doe", "johndoe@example.com",22, new Address("s1",1,"KA")));
        personList.add(new Person(2,"Jane", "Doe", "janedoe@example.com",17, new Address("s2",2,"KA")));
        personList.add(new Person(3,"Jack", "Doe", "jackdoe@example.com",25, new Address("s3",1,"KA")));

        // Return the above list when personRepository.findAll() is called
        Mockito.when(personRepository.findAll()).thenReturn(personList);

        // Perform a GET request to the /api/persons/canVote endpoint
        mockMvc.perform(MockMvcRequestBuilders.get("/api/persons/canVote"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.eligible", Matchers.hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.non-eligible", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.eligible[0].fname", Matchers.is("John")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.eligible[1].fname", Matchers.is("Jack")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.non-eligible[0].fname", Matchers.is("Jane")));
    }
}
